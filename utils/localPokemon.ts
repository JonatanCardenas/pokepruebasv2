import { Pokemon, PokemonList } from "@/interfaces";

const getLocalStorage = (itemLocalStorage: string): PokemonList[] => {
    const pokemonCatch = localStorage.getItem(itemLocalStorage);
    return JSON.parse(pokemonCatch!)
}

const catchNow = (pokemon: Pokemon): boolean => {
    let catpureNow = getLocalStorage('pokemonCatch');
    if (!catpureNow || catpureNow == null) return false;
    var elementOn = catpureNow.filter((pokeCatch) => pokeCatch.id === pokemon.id);
    if (elementOn == null) return false;
    return catpureNow.filter((pokeCatch) => pokeCatch.id === pokemon.id)[0]?.catch;
}

export default {
    getLocalStorage,
    catchNow
}