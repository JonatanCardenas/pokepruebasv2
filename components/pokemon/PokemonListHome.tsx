import { PokemonList } from '@/interfaces';
import { Row, Divider, Space } from "antd";
import { FC, useState } from "react";
import { PokemonCard, PokemonFilter, PokemonSearch } from '.';


interface Props {
  pokemonAll: PokemonList[];
}

const PokemonListHome: FC<Props> = ({ pokemonAll }) => {

  const [pokemons, setPokemons] = useState<PokemonList[]>(pokemonAll);
  const [pokemonsSearch, setPokemonsSearch] = useState<PokemonList[]>([]);
  const [pokemonsFilter, setPokemonsFilter] = useState<PokemonList[]>([]);

  return (
    <>
      <div style={{ textAlign: 'center', margin: '10px', alignItems: 'center', display: 'flex', justifyContent: 'center' }}>



        <PokemonSearch pokemonsFilter={pokemonsFilter} pokemonAll={pokemonAll} setPokemons={setPokemons} setPokemonsSearch={setPokemonsSearch} />
        <PokemonFilter pokemonsSearch={pokemonsSearch} pokemonAll={pokemonAll} setPokemons={setPokemons} setPokemonsFilter={setPokemonsFilter} />

      </div>

      <Divider>Lista de Pokemones</Divider>

      <div style={{
        padding: '10px'
      }}>

        <Row gutter={[8, 8]} >
          {
            pokemons?.map((pokemon) =>
              <PokemonCard poke={pokemon} key={pokemon.id} paramApp={`/Pokemon/${pokemon.id}`} />
            )
          }
        </Row>
      </div>
    </>
  )
}

export default PokemonListHome;