import {Input,Space,Button} from "antd"
import { PokemonList } from '@/interfaces';
import { FC, Dispatch, SetStateAction, useState ,ChangeEvent} from 'react';
import { AudioOutlined, SearchOutlined } from "@ant-design/icons";

const {Search} = Input;

interface Props {
    pokemonsFilter: PokemonList[];
    pokemonAll: PokemonList[];
    setPokemons : Dispatch<SetStateAction<PokemonList[]>>
    setPokemonsSearch : Dispatch<SetStateAction<PokemonList[]>>
}

const PokemonSearch:FC<Props> = ({pokemonsFilter,pokemonAll,setPokemons,setPokemonsSearch}) => {

  //   const [search,setSearch] = useState("")

  //   const onTextFiledChanges = (event: ChangeEvent<HTMLInputElement>) => {

  //     console.log(event.target.value)
  //      setSearch(event.target.value)

  //     if(event.target.value === "")
  //     {
  //       onSearch()
  //     }

  // }

    const onSearch = (value:string) =>{
       
        let pokemonSearch:PokemonList[] = pokemonsFilter.length > 0 ? pokemonsFilter: pokemonAll;   
    
        if(value === "")
        {
          setPokemons(pokemonSearch)
          setPokemonsSearch([])
        }
        else{
          const searchPokemon= pokemonSearch.filter(poke => poke.name.includes(value))
          setPokemons(searchPokemon)
          setPokemonsSearch(searchPokemon)
        }
    
      };

  return (
    <Search placeholder="Ingrese el nombre del pokémon" allowClear onSearch={onSearch} size="large" style={{ 
        width: '100%',
        maxWidth: '350px',
        height: '40px'
        
        
      }} enterKeyHint="enter"/>
    // <>
    // <Space.Compact style={{ width: '100%', alignContent:'center' }}>
    //   <Input  allowClear placeholder="Ingrese el nombre del pokémon" onChange={onTextFiledChanges} />
    //   <Button onClick={onSearch} >
    //     <SearchOutlined />
    //   </Button>
    // </Space.Compact>
    // </>
  )
}

export default PokemonSearch