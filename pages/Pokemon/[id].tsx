import { DetalleLayout } from "@/components/layouts";
import { Pokemon, PokemonCatch, PokemonGQLResponse, PokemonList, PokemonStats } from "@/interfaces";
import { getPokemonInfo, getPokemonInfov2, pokemonStatEnum, pokemonStatNameEnum, pokemonTypesEnum } from "@/utils";
import localPokemon from "@/utils/localPokemon";
import { ArrowLeftOutlined } from "@ant-design/icons";
import { Token } from "@mui/icons-material";
import { Button, Card, Col, Divider, Image, Progress, Row, Switch, Tag, Typography, theme } from "antd";
import axios from "axios";
import { GetStaticPaths, GetStaticProps, NextPage } from "next";
import Link from "next/link";
const { Title } = Typography;
import { useEffect, useState } from 'react';
const { useToken } = theme;

interface Props {
    pokemon: PokemonList
}
const PokemonDetalle: NextPage<Props> = ({ pokemon }) => {
    console.log("pokemon23", pokemon)
    console.log("pokemon", pokemon.types)

    const [capture, setCapture] = useState(false);
    const tipopokemon: string = pokemon.types[0];
    const colorPokemon: string = pokemonTypesEnum[tipopokemon as keyof typeof pokemonTypesEnum]
    const { token } = useToken();
    console.log({ pokemon });
    const onColor = (stat: string) => {
        return pokemonStatEnum[stat as keyof typeof pokemonStatEnum]
    }

    const catchNow = (): boolean => {
        let catpureNow = localPokemon.getLocalStorage('pokemonCatch');
        if (!catpureNow || catpureNow == null) return false;
        var elementOn = catpureNow.filter((pokeCatch) => pokeCatch.id === pokemon.id);
        if (elementOn == null) return false;
        return catpureNow.filter((pokeCatch) => pokeCatch.id === pokemon.id)[0]?.catch;
    }
    const onStateName = (stat: string) => {
        return pokemonStatNameEnum[stat as keyof typeof pokemonStatNameEnum]
    }
    useEffect(() => {

        setCapture(catchNow)
    }, [])
    const catchPokemon = () => {

        const pokemonCatch = localPokemon.getLocalStorage('pokemonCatch');
        let pokemonCatchs: PokemonList[] = [];
        let pokemonValue: PokemonList = {
            catch: !capture,
            name: pokemon.name,
            id: pokemon.id,
            regions: pokemon.regions,
            types: pokemon.types,
            img: pokemon.img,
            height: pokemon.height,
            stats: pokemon.stats,
            weight: pokemon.weight
        }
        setCapture(!capture);
        if (pokemonCatch == null
            || pokemonCatch.length == 0) {
            console.log("nuevo")
            pokemonCatchs.push(pokemonValue);
            localStorage.setItem('pokemonCatch', JSON.stringify(pokemonCatchs));
        } else {
            localStorage.removeItem("pokemonCatch");
            pokemonCatchs = pokemonCatch.filter((pokeCatch) => pokeCatch.id !== pokemon.id);
            pokemonCatchs.push(pokemonValue);
            localStorage.setItem('pokemonCatch', JSON.stringify(pokemonCatchs));

        }

    }
    useEffect(() => {

    }, [])
    return (
        <DetalleLayout title={pokemon.name}>
            <Row gutter={[24, 24]} style={{ marginRight: '0px', background: colorPokemon }} >
                <Col span={12}>
                    <Link href='/' passHref >
                        <Button type="text" shape='circle' icon={<ArrowLeftOutlined style={{ fontWeight: 'bolder', color: token.colorIcon }} />} />
                        <Typography.Text strong style={{ fontWeight: 'bolder', paddingLeft: '10px' }}>Pokedex</Typography.Text >
                    </Link>
                </Col>
                <Col span={12} className='gutter-row' >
                    <div style={{ display: 'flex', justifyContent: 'end', paddingRight: '10px', paddingTop: '5px' }}>
                        <Typography.Text strong style={{ fontWeight: 'bolder' }}>#{pokemon.id < 10 ? `00${pokemon.id}` : pokemon.id < 100 ? `0${pokemon.id}` : pokemon.id}</Typography.Text >
                    </div>
                </Col>
            </Row>
            <Row >
                <Col style={{ display: 'flex', justifyContent: 'center', background: colorPokemon, borderRadius: '0px 0px 40px 40px', width: '100%', maxHeight: '300px', minHeight: '200px' }} >
                    <Image
                        width={200}
                        src={pokemon.img}
                        alt={pokemon.name}
                    />
                </Col>
            </Row>
            <Row justify={'center'}>
                <Title >
                    {pokemon.name}
                </Title>
            </Row>
            <Row justify={'center'}>
                {
                    pokemon.types.map((type) => (
                        <Col key={type} >
                            <Tag color={pokemonTypesEnum[type as keyof typeof pokemonTypesEnum]} bordered style={{ width: '100px', textAlign: 'center', borderRadius: '10px' }}>
                                {type}
                            </Tag>
                        </Col>

                    ))
                }
            </Row>
            <Row>
                <Col span={12}>
                    <Row >
                        <Col span={24} style={{ textAlign: 'center' }}>
                            <Typography.Title level={3} style={{ marginBlockEnd: '0em', fontWeight: 'bolder' }}  >{pokemon.weight} KG</Typography.Title>
                            <Typography.Title level={5} style={{ marginBlockEnd: '0em', marginBlockStart: '0em', color: 'gray' }}>Weight</Typography.Title>
                        </Col >
                    </Row>
                </Col>
                <Col span={12}>
                    <Row >
                        <Col span={24} style={{ textAlign: 'center' }}>
                            <Typography.Title level={3} style={{ marginBlockEnd: '0em', fontWeight: 'bolder' }} >{pokemon.height}{pokemon.height}  M</Typography.Title>
                            <Typography.Title level={5} style={{ marginBlockEnd: '0em', marginBlockStart: '0em', color: 'gray' }}>Height</Typography.Title>
                        </Col >
                    </Row>
                </Col>
            </Row>
            <Row style={{ padding: '25px 0px' }}>
                <Col span={24} style={{ textAlign: 'center' }}>
                    <Typography.Title >
                        Base Stats
                    </Typography.Title>
                </Col>
                {
                    pokemon.stats.filter(x => x.stat != 'speed').map((stat) => (
                        <Col span={24} key={stat.stat}>
                            <Row >
                                <Col span={2} style={{ textAlign: 'center' }}>
                                    <Typography.Text >{onStateName(stat.stat)}</Typography.Text>
                                </Col>
                                <Col span={20}>
                                    <Progress percent={stat.base} strokeColor={onColor(onStateName(stat.stat))} showInfo={false} strokeWidth={13} />
                                </Col>
                            </Row>
                        </Col>

                    ))}
            </Row>

            <Divider style={{ background: 'white' }} />
            <Row style={{ padding: '10px' }} justify={'end'} >
                <Col span={24} style={{ textAlign: 'center' }} >
                    <Typography.Title style={{ paddingRight: '10px' }} level={4} >CATCH</Typography.Title>
                    <Switch checked={capture} onChange={catchPokemon} />
                </Col>
            </Row>

        </DetalleLayout>
    )
}

export const getStaticPaths: GetStaticPaths = async (ctx) => {
    const pokemons151 = [...Array(151)].map((value, index) => `${index + 1}`);
    return {
        paths: pokemons151.map(id => ({
            params: { id }
        })),
        fallback: 'blocking' //bloking va dar la impresion de que la carg
    }

}

export const getStaticProps: GetStaticProps = async ({ params }) => {

    const { id } = params as { id: string };


    const pokemon = await getPokemonInfov2(id);
    console.log('pokemon2', pokemon.pokemon);
    if (!pokemon) {
        return {
            redirect: {
                destination: '/',
                permanent: false     //regrresion permanente o no lo es?  => la redireccion  la vamos a borrar del limite.
            }
        }
    }
    return {
        props: {
            pokemon: pokemon.pokemon
        }
    }
}
export default PokemonDetalle;