import React from 'react'
import pokebola from '../public/img/pokeball.gif';
import Image from 'next/image';
const Loading = () => {
    return (
        <div style={{width:'100%',display:'flex',justifyContent:'center',height:'100vh',alignItems:'center',background:'black'}}>       
            <Image src={pokebola} alt='logo pokemón' style={{width:'200px',height:'200px'}}/>
        </div>
    )
}

export default Loading